import { createServer } from 'vite';
import createVuePlugin from '@vitejs/plugin-vue';

(async () => {
    const server = await createServer({
        root: './src',
        plugins: [
            createVuePlugin()
        ]
    });
    console.log(`Server listening on port ${(await server.listen()).config.server.port}`);
})().catch(console.error);