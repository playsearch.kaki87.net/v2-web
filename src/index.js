import { createApp } from 'vue';
import { createEmitter } from 'simpler-emitter';
import * as Sentry from '@sentry/vue';

import { sentryDsn } from '../config.js';

import App from './components/App.vue';

const
    app = createApp(App),
    emitter = createEmitter();

if(sentryDsn){
    Sentry.init({
        app,
        dsn: sentryDsn,
        logErrors: true,
        beforeSend: event => {
            if(event.exception){
                emitter.emit(
                    'exceptionCaptured',
                    {
                        event,
                        showReportDialog: () => Sentry.showReportDialog({ eventId: event.event_id })
                    }
                );
            }
            return event;
        }
    });
}

app.config.globalProperties.emitter = emitter;

app.mount('.App');