# `@playsearch.kaki87.net/v2-web` - Advanced Google Play Search

[![](https://shields.kaki87.net/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2Fserver-KaTys%2Fstatus%2Fmaster%2Fapi%2Fka-ki87-play-search%2Fuptime.json)](https://status.katys.eu.org/history/ka-ki87-play-search)
[![](https://shields.kaki87.net/discord/739600823415472128)](https://discord.gg/XZ8Ja8d)

Filter & sort Google Play search results.

## Features

### Search

Powered by [`google-play-scraper`](https://github.com/facundoolano/google-play-scraper),
search works exactly like opening the Google Play App, typing and submitting.

Therefore, **empty search is not possible**.

Search will return up to 250 results loading in real-time.

### Results

Results are shown in a table which has two views : apps & permissions.

#### Results apps view

Apps view allows sorting results by :
index, name, category, install count, score, size, last update date, price, presence of advertisements,
presence of in-app purchases, minimal android version requirement and permission count.

Additional info is available as tooltips :
ID on index, editor on name, rating count on score, relative time on last update date.

Apps are guaranteed to be available from the Play Store, but additional links are generated :
[APKPure](https://apkpure.com/), [F-Droid](https://f-droid.org/en/), and [Aptoide](https://en.aptoide.com/).

| ![](https://i.goopics.net/1jeg1h.png) | ![](https://i.goopics.net/klhane.png) |
|---------------------------------------|---------------------------------------|

#### Results permissions view

Permissions view allows comparing results by permission requirements.

![](https://i.goopics.net/8v0f6s.png)

### Filters

Filters allow filtering out apps by :
price, presence of advertisements, presence of in-app purchases, category, minimal android version requirement and description.

Description filter allows : including a single word with `word`, including a phrase with `"multiple words"`,
excluding a single word with `-word` and excluding a phrase with `-"multiple words"`.

This Google-ish feature is powered by [`psq`](https://github.com/swang/psq).

## [Changelog](./CHANGELOG.md)

## Misc

### Similar projects

- ~~[appfilter.net](https://appfilter.net/) (proprietary)~~ *Down*
- [simplemobiletools.com](https://www.simplemobiletools.com/) (not a search engine but a curated apps list)
- [play.gqqnbig.me](https://play.gqqnbig.me/) ([open source](https://github.com/gqqnbig/advanced-play-search))
- [appbrain.com](https://www.appbrain.com/) (proprietary)

### Featured in

- [El buscador que hace lo que Google no quiere: filtrar al máximo las búsquedas de Play Store](https://www.xatakandroid.com/aplicaciones-android/buscador-que-hace-que-google-no-quiere-filtrar-al-maximo-busquedas-play-store) (2022-12-08)
- [Android - katere aplikacije @ Slo-Tech](https://slo-tech.com/forum/t400090/2349) (2022-11-24)
- [Advanced Google Play Search - Ailleurs sur le web](https://ardechelibre.org/liens/shaare/2MY1ig) (2022-10-18)
- [Privacy respecting weather app for Android](https://news.ycombinator.com/item?id=33229727) (2022-10-17)
- [Telegram-канал r_internetisbeautiful - r/InternetIsBeautiful: Unsorted - каталог телеграмм](https://en.tgchannels.org/channel/r_internetisbeautiful?start=8139) (2022-06-27)
- [Buscador de apps sin anuncios en la PlayStore de android | Mediavida](https://www.mediavida.com/foro/electronica-telefonia/buscador-apps-sin-anuncios-playstore-android-685339) (2022-04-06)
- [Ako hľadať na Androide iba aplikácie bez reklám a zadarmo - Tipy Triky TOUCHIT](https://tipy.touchit.sk/ako-hladat-na-androide-iba-aplikacie-bez-reklam-a-zadarmo/752/) (2022-01-08)
- [Tell HN: You can't add “no ads” in your Play Store app's title | Hacker News](https://news.ycombinator.com/item?id=29497680) (2021-12-09)
- [KPlaySearch | Advanced Google Play search - DPSBookmarks](https://bookmarks.davidinformatico.com/shaare/1J0mhg) (2021-05-11)
- [How to write and publish your next successful Android app](https://scribe.rip/how-to-write-and-publish-your-next-successful-android-app-3632140e3bc1) (2020-10-13)
- [Кака для Серёги Брина — Games — Форум](https://www.linux.org.ru/forum/games/15848047) (2020-08-09)
- [Google Play Store: Neue Suchfilter erreichen mehr Nutzer](https://stadt-bremerhaven.de/google-play-store-neue-suchfilter-erreichen-mehr-nutzer/#comment-1108723) (2020-07-27)
- [Поиск приложений для смартфона](http://ewgenik.ru/page/poisk-prilozhenij-dlja-smartfona) (2020-07-05)
- [Spatry's Favorite Android Apps 2020: Package Management - YouTube](https://www.youtube.com/watch?v=liGsILtvUqM&t=905s) (2020-05-06)
- [Android App Addicts #564 – Snapdrop, Miradore and GetHuman | Podnutz](https://podnutz.com/aaa564/) (2020-05-03)
- [Софт для Android | Сторінка 398 | Red Forum](https://red-forum.com/threads/%D0%A1%D0%BE%D1%84%D1%82-%D0%B4%D0%BB%D1%8F-android.16000/page-398#post-1541002) (2019-12-12)
- [Flatpak 1.5.1 Prepares For Protected/Authenticated Downloads - Future App Purchasing - Phoronix Forums](https://www.phoronix.com/forums/forum/software/desktop-linux/1141891-flatpak-1-5-1-prepares-for-protected-authenticated-downloads-future-app-purchasing/page4#post1142056) (2019-11-28)
- [Xiaomi Mi A3 - Page 35 - Xiaomi Mi phones - myPhone](https://myphone.gr/forum/topic/178099/page/35/?tab=comments#comment-6727346) (2019-11-13)
- [Google Play Store : la toute nouvelle interface Material est officielle · Frandroid · Disqus](https://disqus.com/home/discussion/frandroid/google_play_store_la_toute_nouvelle_interface_material_est_officielle/#comment-4587615111) (2019-08-22)
- [applications - How to search for Android apps without ads? - Android Enthusiasts Stack Exchange](https://android.stackexchange.com/a/213547) (2019-06-12)
- [[cubacel] buscador de aplicaciones mejor que el de Google Play - cubacel - FreeLists](https://www.freelists.org/post/cubacel/buscador-de-aplicaciones-mejor-que-el-de-Google-Play) (2018-10-06)
- [Android - start.me](https://start.me/p/4Kwgzq/android)
- [HomePages/MESHUGGAH/Lab - TASVideos](https://tasvideos.org/HomePages/MESHUGGAH/Lab)
- [Designing an Interactive Application to Promote Healthy Ageing among Older Adults at Home](https://pats.cs.cf.ac.uk/@archive_file?p=1706&n=final&f=1-report.pdf&SIG=dd1c828d73e15a63eda5d3ed3ae1c18e2c9f80cc257cf1e1125e532a4863979a)