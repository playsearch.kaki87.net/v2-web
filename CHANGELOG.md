# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.0 (2022-01-21)

Initial release

### Differences from [v1](https://git.kaki87.net/playsearch.kaki87.net/v1)

#### User interface

- Layout enhanced :
  - Title moved from own section to search query input placeholder
  - Filters moved from own section to footer
  - Vertical scroll for filters on small screens
  - App icon moved to own column
  - App version moved to updated date tooltip
- Extra icons from [`Fork-Awesome`](https://github.com/ForkAwesome/Fork-Awesome) instead of [`Font-Awesome`](https://github.com/FortAwesome/Font-Awesome)

#### User experience

- Search & filter flow enhanced
  - Search results returned in real time using [Server-sent events](https://developer.mozilla.org/fr/docs/Web/API/Server-sent_events)
  - Filters availability, options and ranges computed after search
  - Unlimited filters update without search re-run
  - App description filter added
  - *Reset* button removed
- *About* link redirected to [`README.md`](./README.md)
- App index moved to own sortable column, app ID added as tooltip
- Google Play link moved to own column, extra links added
- Permissions table inverted, app icon added

#### Development

- Web app (this repo) is separate from the API client ([repo](https://git.kaki87.net/playsearch.kaki87.net/v2-client)) and the API server ([repo](https://git.kaki87.net/playsearch.kaki87.net/v2-api)).
- Dates are parsed & displayed by [`dayjs`](https://github.com/iamkun/dayjs)
- Semver version numbers compared using [`compare-versions`](https://github.com/omichelsen/compare-versions)
- Size strings compared using [`numbro`](https://github.com/BenjaminVanRyseghem/numbro)