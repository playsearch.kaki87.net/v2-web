/**
 * API URL
 * @type {String}
 */
export const apiUrl = null;

/**
 * Sentry DSN
 * @type {String}
 */
export const sentryDsn = null;